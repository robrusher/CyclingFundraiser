#!/bin/bash

PLIST=platforms/ios/*/*-Info.plist

cat << EOF |
Set :NSPhotoLibraryUsageDescription string 'Save shared pictures to your library.'
Set :NSCameraUsageDescription string 'Take picture that you can then share.'
Set :NSLocationWhenInUseUsageDescription string 'Show current location on the map.'
Add :NSAppTransportSecurity:NSExceptionDomains:secure.childrenscoloradofoundation.org:NSExceptionRequiresForwardSecrecy bool NO
Delete :ITSAppUsesNonExemptEncryption
Add :ITSAppUsesNonExemptEncryption bool NO
EOF
while read line
do
    /usr/libexec/PlistBuddy -c "$line" $PLIST
done

true
