angular.module('courage.controllers', [])

.controller('AppCtrl', function($scope, $rootScope, $ionicPlatform, $ionicPopup, userService, $timeout, $cordovaSocialSharing, $cordovaScreenshot, $ionicPopover, $cordovaCamera, ConnectivityMonitor) {

    var msg = "I\'m riding in the Courage Classic bicycle tour. Join me in supporting Children\'s Hospital Colorado."

    $ionicPopover.fromTemplateUrl('templates/popover.html', {
      scope: $scope
    }).then(function(popover) {
      $scope.popover = popover;
    });

    $scope.openPopover = function($event) {
      $scope.popover.show($event);
    };

    $scope.closePopover = function() {
      $scope.popover.hide();
    };

    //Cleanup the popover when we're done with it!
    $scope.$on('$destroy', function() {
      $scope.popover.remove();
    });

  $scope.shareMessage = function(){
    $scope.closePopover();
    if(ConnectivityMonitor.isOnline()) {
      var user = userService.get();
      var donationUrl = '';

      if(user && user.donationUrl) {
        donationUrl = user.donationUrl + '&utm_source=mobile_app&utm_campaign=courage_classic';
      }

      setTimeout(function(){
        $cordovaSocialSharing.share(msg,"Courage Classic", null, donationUrl);
      }, 1500);

    } else {
      $scope.showAlert();
    }
  };

  $scope.shareScreenshot = function(){
    $scope.closePopover();
    setTimeout(function(){
      if(ConnectivityMonitor.isOnline()) {
        var user = userService.get();
        var donationUrl = '';

        if(user && user.donationUrl) {
          donationUrl = user.donationUrl + '&utm_source=mobile_app&utm_campaign=courage_classic';
        }

        $cordovaScreenshot.capture()
        .then(function(result) {
          //on success you get the image url
          //post on facebook (image & link can be null)
          $cordovaSocialSharing.share(msg,"Courage Classic", result, donationUrl)
          .then(function(result) {
            //do something on post success or ignore it...
            }, function(err) {
              console.log("There was an error sharing!");
            });
          }, function(err) {
            console.log("There was an error taking a screenshot!");
        });
      } else {
        $scope.showAlert();
      }
    }, 1500);

  };

  $scope.sharePhoto = function(){
    $scope.closePopover();
    if(ConnectivityMonitor.isOnline()) {
      var user = userService.get();
      var donationUrl = '';

      if(user && user.donationUrl) {
        donationUrl = user.donationUrl + '&utm_source=mobile_app&utm_campaign=courage_classic';
      }

      var options = {
        quality: 50,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.CAMERA,
        allowEdit: true,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 320,
        targetHeight: 320,
        saveToPhotoAlbum: true,
  	    correctOrientation:true
      };
      $cordovaCamera.getPicture(options)
      .then(function(imageData) {
        //var image = document.getElementById('myPhoto');
        //image.src = "data:image/jpeg;base64," + imageData;
        //on success you get the image url
        //post on facebook (image & link can be null)
        $cordovaSocialSharing.share(msg,"Courage Classic", "data:image/jpeg;base64," + imageData, donationUrl)
        .then(function(result) {
          //do something on post success or ignore it...
          }, function(err) {
            console.log("There was an error sharing!");
          });
        }, function(err) {
          console.log("There was an error taking a picture!");
        });
    } else {
      $scope.showAlert();
    }
  };

  $scope.showAlert = function() {
    $ionicPopup.alert({title:"Network Not Available",template:"Social features are not available while offline."});
  };

  $scope.showAbout = function() {
      var alertPopup = $ionicPopup.alert({
          title: 'Courage Classic App',
          template: '<div class="agendaItems centered">Brought to you by</div>'+
          '<div class="detailItems centered">The Special Events team at<br/>'+
          'Children\'s Hospital Colorado Foundation</div>'+
          '<div class="centered">Information Line: <a href="tel:720-777-7499">720-777-7499</a></div>'+
          '<br/><div class="agendaItems centered">Development By:</div>'+
          '<a href="#" onclick="window.open(\'http://on3software.com\', \'_system\');"><img class="centered" src="img/18.png"/></a>'+
          '<a href="tel:303-885-7044" class="centered">App Tech Support</a>'+
          '<div class="noteItems centered">Copyright &copy; 2017<br/>'+
          'Children\'s Hospital Colorado Foundation<br/>'+
          'Version 4.0.8</div>'
      });
  };

  $scope.goToURL = function (url) {
    if(window.cordova && window.cordova.InAppBrowser){
      window.cordova.InAppBrowser.open(url, "_blank", 'location=yes');
    }else{
      window.open(url,'_blank','location=yes');
    }
  };

});
