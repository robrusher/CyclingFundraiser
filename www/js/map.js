angular.module('courage.controllers')
.controller('LiveMapsCtrl', function ($ionicPlatform, $scope, $http, leafletData, $ionicTabsDelegate, dailyData, $rootScope, ParseService) {
    var liveMap = this;
    var sheetID = 1;
    var prevRoute;
    // create layer for markers
    var markers = new L.layerGroup();
    var rider = new L.layerGroup();

    $scope.defaults = {
      tileLayer: 'http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png',
      //tileLayer: '/img/mapTiles/{z}/{x}/{y}.png',
      minZoom: 10,
      maxZoom: 17,
      zoomControl:false,
      tileLayerOptions: {
          detectRetina: true
      }
    };
    // var maxbounds = leafletBoundsHelpers.createBoundsFromArray([
    //  [ 39.234381, -106.514511 ],
    //  [ 39.687110, -105.869064 ]
    // ]);
    // angular.extend($scope, {
    //   maxbounds: maxbounds
    // });

    $ionicPlatform.ready(function () {
        console.log('Online: '+$rootScope.online)
        if($rootScope.online){
          ParseService.getRoutes().then(function(data) {
            $scope.routes = data;
            window.localStorage.setItem("routes", JSON.stringify(data));
          }).catch(function(error) {
            console.log(error);
          });
          ParseService.getStations().then(function(data) {
            $scope.stations = data;
            window.localStorage.setItem("stations", JSON.stringify(data));
          }).catch(function(error) {
            console.log(error);
          });
            // Call the async method and then use the agenda inside the then function
            dailyData.async(8).then(function(d) {
                // $cordovaFile.writeFile(cordova.file.dataDirectory, "stationData.txt", JSON.stringify(d), true).then(function (success) {
                //     // success
                //     console.log('file created')
                // }, function (error) {
                //     // error
                //     console.log('ERROR 1: '+JSON.stringify(error));
                // });

                liveMap.sdata = d;
                liveMap.skeys = Object.keys(d);
            }).finally(function(){
                dailyData.async(sheetID).then(function(d) {
                    // $cordovaFile.writeFile(cordova.file.dataDirectory, "routeData.txt", JSON.stringify(d), true).then(function (success) {
                    //     // success
                    //     console.log('file created')
                    // }, function (error) {
                    //     // error
                    //     console.log('ERROR 2: '+JSON.stringify(error))
                    // });

                    liveMap.data = d;
                    liveMap.keys = Object.keys(d);
                }).finally(function(){
                    var today = new Date().toDateString();
                    var day1 = new Date(2015,6,18).toDateString();
                    var day2 = new Date(2015,6,19).toDateString();
                    var day3 = new Date(2015,6,20).toDateString();

                    switch(today){
                        case day3:
                            liveMap.selectDay(liveMap.keys[2]);
                            liveMap.currentEvents = liveMap.data['MON'];
                            break;
                        case day2:
                            liveMap.selectDay(liveMap.keys[1]);
                            liveMap.currentEvents = liveMap.data['SUN'];
                            break;
                        default:
                            liveMap.selectDay(liveMap.keys[0]);
                            liveMap.currentEvents = liveMap.data['SAT'];
                    }
                })
            })

        } else {
            console.log('entered offline code');
          // if($cordovaFile.checkFile(cordova.file.dataDirectory, "routeData.txt")) {
          //   $cordovaFile.readAsText(cordova.file.dataDirectory, "routeData.txt").then(function (success) {
          //       var cachedData = JSON.parse(success);
          //       liveMap.data = cachedData;
          //       liveMap.keys = Object.keys(cachedData);
          //       liveMap.selectDay(liveMap.keys[0]);
          //
          //       $cordovaFile.readAsText(cordova.file.dataDirectory, "stationData.txt").then(function (success) {
          //           var cachedSData = JSON.parse(success);
          //           liveMap.sdata = cachedSData;
          //           liveMap.skeys = Object.keys(cachedSData);
          //       }, function (error) {
          //           console.log('ERROR 4: '+JSON.stringify(error))
          //       });
          //
          //       var today = new Date().toDateString();
          //       var day1 = new Date(2016,6,23).toDateString();
          //       var day2 = new Date(2016,6,24).toDateString();
          //
          //       switch(today){
          //           case day2:
          //               liveMap.selectDay(liveMap.keys[1]);
          //               liveMap.currentEvents = liveMap.data['SUN'];
          //               break;
          //           default:
          //               liveMap.selectDay(liveMap.keys[0]);
          //               liveMap.currentEvents = liveMap.data['SAT'];
          //       }
          //   }, function (error) {
          //       console.log('ERROR 3: '+JSON.stringify(error))
          //   });
          // }
        };
    });

    // define elevation control
    var el = L.control.elevation({
        position: "bottomright",
        theme: "steelblue-theme", //default: lime-theme
        width: window.innerWidth - 20,
        height: 75,
        margins: {
            top: 10,
            right: 20,
            bottom: 20,
            left: 57
        },
        useHeightIndicator: true, //if false a marker is drawn at map position
        interpolation: "linear", //see https://github.com/mbostock/d3/wiki/SVG-Shapes#wiki-area_interpolate
        hoverNumber: {
            decimalsX: 3, //decimals on distance (always in km)
            decimalsY: 0, //deciamls on height (always in m)
            formatter: undefined //custom formatter function may be injected
        },
        xTicks: undefined, //number of ticks in x axis, calculated by default according to width
        yTicks: undefined, //number of ticks on y axis, calculated by default according to height
        collapsed: false,    //collapsed mode, show chart on click or mouseover
        imperial: true
    });

    // add elevation control to map
    leafletData.getMap().then(function(map) {
        el.addTo(map);
        map.addLayer(markers);
        map.addLayer(rider);
        map.attributionControl.setPrefix('');
    });

    var base64shadow = 'img/markers/marker-shadow.png';

    // define icons
    var local_icons = {
        finish: {
          iconUrl: 'img/markers/flag.png',
          retinaUrl: 'img/markers/flag@2x.png',
          shadowUrl: base64shadow,
          iconSize: [28, 40],
          shadowSize: [40, 50],
          iconAnchor: [14, 40],
          shadowAnchor: [14, 50],
          popupAnchor: [0, -40]
        },
        lunch: {
          iconUrl: 'img/markers/life-saver.png',
          retinaUrl: 'img/markers/life-saver@2x.png',
          shadowUrl: base64shadow,
          iconSize: [28, 40],
          shadowSize: [40, 50],
          iconAnchor: [14, 40],
          shadowAnchor: [14, 50],
          popupAnchor: [0, -40]
        },
        aid: {
          iconUrl: 'img/markers/red-cross.png',
          retinaUrl: 'img/markers/red-cross@2x.png',
          shadowUrl: base64shadow,
          iconSize: [28, 40],
          shadowSize: [40, 50],
          iconAnchor: [14, 40],
          shadowAnchor: [14, 50],
          popupAnchor: [0, -40]
        },
        rider: {
          iconUrl: 'img/markers/blue.png',
          retinaUrl: 'img/markers/blue@2x.png',
          shadowUrl: base64shadow,
          iconSize: [28, 40],
          shadowSize: [40, 50],
          iconAnchor: [14, 40],
          shadowAnchor: [14, 50],
          popupAnchor: [0, -40]
        },
        relief: {
          iconUrl: 'img/markers/relief.png',
          retinaUrl: 'img/markers/relief@2x.png',
          shadowUrl: base64shadow,
          iconSize: [28, 40],
          shadowSize: [40, 50],
          iconAnchor: [14, 40],
          shadowAnchor: [14, 50],
          popupAnchor: [0, -40]
        },
        picture: {
          iconUrl: 'img/markers/camera.png',
          retinaUrl: 'img/markers/camera@2x.png',
          shadowUrl: base64shadow,
          iconSize: [28, 40],
          shadowSize: [40, 50],
          iconAnchor: [14, 40],
          shadowAnchor: [14, 50],
          popupAnchor: [0, -40]
        }
    };

    liveMap.selectDay = function (day) {
        this.selectedDay = day;
        if( liveMap.data ){
            liveMap.currentEvents = liveMap.data[day];
            //liveMap.currentEventsTime = Object.keys(liveMap.currentEvents);
            setTimeout(function(){
                $ionicTabsDelegate.$getByHandle('options').select(0);
                liveMap.loadGeoJSON(0);
            },100);
        }
    };

    liveMap.isActive = function (day) {
        return day === this.selectedDay;
    };

    liveMap.centerJSON = function() {
      console.log('center the map');
      leafletData.getMap().then(function(map) {
        var latlngs = [];
        for (var i in $scope.geojson.data.features[0].geometry.coordinates) {
          var coord = $scope.geojson.data.features[0].geometry.coordinates[i];
            //coord.pop();
            latlngs.push(L.GeoJSON.coordsToLatLng(coord));
        }
        map.fitBounds(latlngs,{padding:[20,20]});
      });
    };

    liveMap.getRiderLocation = function() {
        navigator.geolocation.getCurrentPosition(function(pos) {
            rider.clearLayers();
            var icon = L.icon(local_icons.rider);
            var you = new L.marker([pos.coords.latitude, pos.coords.longitude], {title: 'You are here.', icon: icon});
            rider.addLayer(you);
        }, function(error) {
            alert('Unable to get location: ' + error.message);
        });
    };

    liveMap.addMarkers = function(set) {
            leafletData.getMap().then(function(map) {
                markers.clearLayers();
                var markerset = liveMap.sdata[set];
                for(var n in markerset) {
                    var icon = L.icon(local_icons[markerset[n].gsx$type.$t]);
                    var marker = new L.marker([markerset[n].gsx$lat.$t,markerset[n].gsx$lng.$t], {icon: icon});
                    var popup = L.popup({maxWidth:300}).setContent(markerset[n].gsx$displaytext.$t + '<br/>'+
                                     markerset[n].gsx$open.$t + ' to ' +
                                     markerset[n].gsx$close.$t);
                    marker.bindPopup(popup);
                    markers.addLayer(marker);
                }
            })
        liveMap.getRiderLocation();
    };

    // Get the countries geojson data from a JSON
    liveMap.loadGeoJSON = function(index) {
        $ionicTabsDelegate.select(index);
        var dayRoute = liveMap.currentEvents[index].gsx$file.$t;
        var dayColor = liveMap.currentEvents[index].gsx$color.$t;
        var fileName = "geojson/"+dayRoute;
        // clear elevation control if different route
        //if(dayRoute != prevRoute)
        el.clear();
        prevRoute = dayRoute;
        $http.get(fileName).success(function(data, status) {
          angular.extend($scope, {
            geojson: {
              data: data,
              style: {
                weight: 5,
                opacity: 1,
                color: dayColor
              },
              onEachFeature: el.addData.bind(el) // bind data to elevation control
            }
          });
        }).finally(function() {
          liveMap.addMarkers(liveMap.currentEvents[index].gsx$markerset.$t);
          liveMap.centerJSON();
        });
    };

    angular.element(document).ready(function () {
        liveMap.selectDay('SAT');
    });
});
