angular.module('courage.controllers')
.controller('DiningCtrl', function ($rootScope, $cordovaFile, $ionicPlatform, ParseService, ConnectivityMonitor) {
    var dining = this;
    var sheetID = 4;

    $ionicPlatform.ready(function () {

      switch (ConnectivityMonitor.isOnline()){
        // ONLINE Branch
        case true:
          // Get live data for routes
          ParseService.getRestaurants().then(function(data) {
            dining.data = data;
            dining.keys = Object.keys(data);
            dining.selectDay(dining.keys[0]);
          }).catch(function(error) {
            console.log(error);
          });
        break;

      // OFFLINE Branch
        case false:
          console.log('entered offline code');

          var cachedData = window.localStorage.getItem("restaurants");
          dining.data = cachedData;
          dining.keys = Object.keys(cachedData);
          dining.selectDay(dining.keys[0]);
        break;
      }
    });

    dining.selectDay = function(day) {
        this.selectedDay = day;
        dining.currentEvents = dining.data[this.selectedDay];
        //dining.currentEventsTime = Object.keys(dining.currentEvents);
    }

    dining.isActive = function (day) {
        return day === this.selectedDay;
    }

    dining.toggleGroup = function(items) {
        if (dining.isGroupShown(items)) {
            dining.shownGroup = null;
        } else {
            dining.shownGroup = items;
        }
    }

    dining.isGroupShown = function(items) {
        return dining.shownGroup === items;
    }
});
