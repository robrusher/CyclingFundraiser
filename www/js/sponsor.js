angular.module('courage.controllers')
.controller('SponsorCtrl', function ($rootScope, $cordovaFile, $ionicPlatform, ParseService, ConnectivityMonitor) {
    var sponsor = this;
    var sheetID = 5;

    $ionicPlatform.ready(function () {

      switch (ConnectivityMonitor.isOnline()){
        // ONLINE Branch
        case true:
          // Get live data for routes
          ParseService.getSponsors().then(function(data) {
            sponsor.data = data;
            sponsor.categories = Object.keys(data);
          }).catch(function(error) {
            console.log(error);
          });
        break;

      // OFFLINE Branch
        case false:
          console.log('entered offline code');

          var cachedData = window.localStorage.getItem("sponsors");
          sponsor.data = cachedData;
          sponsor.categories = Object.keys(cachedData);
        break;
      }
    });

    sponsor.categoryItems = function (category) {
        return sponsor.data[category];
    }
});
