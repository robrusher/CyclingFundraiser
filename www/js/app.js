angular.module('courage', ['ionic','ionic.cloud','courage.controllers','ui-leaflet','ngCordova'])

.run(function($ionicPlatform, $ionicPush, $state, $rootScope) {

  $ionicPlatform.ready(function() {
    Parse.initialize("courageClassic");
    Parse.serverURL = '<your parse server url>';
    $rootScope.sessionUser = Parse.User.current();

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    $ionicPush.register().then(function(t) {
      return $ionicPush.saveToken(t);
    }).then(function(t) {
      console.log('Token saved:', t.token);
    });

    $rootScope.volunteer = getDataCache("volunteer");
  });

  $rootScope.$on('cloud:push:notification', function(event, data) {
    alert(data.message.title + ': ' + data.message.text);
  });

})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, $ionicCloudProvider) {
  $ionicCloudProvider.init({
    "core": {
      "app_id": "<your ionic app id>"
    },
    "push": {
      "sender_id": "<your sender id>",
      "pluginConfig": {
        "ios": {
          "badge": true,
          "sound": true
        },
        "android": {
          "iconColor": "#343434"
        }
      }
    }
  });

  $ionicConfigProvider.tabs.position('bottom');

  $stateProvider
    .state('app', {
      url: "/app",
      abstract: true,
      templateUrl: "templates/menu.html",
      controller: 'AppCtrl'
    })

    .state('app.agenda', {
      url: "/agenda",
      views: {
        'menuContent': {
          templateUrl: "templates/agenda.html",
          controller: 'AgendaCtrl as agenda'
        }
      }
    })

    .state('app.activity', {
      url: "/activity",
      views: {
        'menuContent': {
          templateUrl: "templates/activity.html",
          controller: 'ActivityCtrl as activity'
        }
      }
    })

    .state('app.dining', {
      url: "/dining",
      views: {
        'menuContent': {
          templateUrl: "templates/dining.html",
          controller: 'DiningCtrl as dining'
        }
      }
    })

    .state('app.livemap', {
      url: "/livemap",
      views: {
        'menuContent': {
          templateUrl: "templates/livemap.html",
          controller: 'LivemapCtrl as liveMap'
        }
      }
    })

    .state('app.sponsors', {
      url: "/sponsors",
      views: {
        'menuContent': {
          templateUrl: "templates/sponsors.html",
          controller: 'SponsorCtrl as sponsor'
        }
      }
    })

    .state('app.alerts', {
      cache: false,
      url: "/alerts",
      views: {
        'menuContent': {
          templateUrl: "templates/alerts.html",
          controller: 'AlertsCtrl as alerts'
        }
      }
    })

    .state('app.faq', {
      url: "/faq",
      views: {
        'menuContent': {
          templateUrl: "templates/faq.html",
          controller: 'FAQCtrl as faq'
        }
      }
    })

    .state('app.settings', {
      url: "/settings",
      views: {
        'menuContent': {
          templateUrl: "templates/settings.html",
          controller: 'AppSettingsCtrl as appSettings'
        }
      }
    })

    .state('app.volunteer', {
      url: "/volunteer",
      views: {
        'menuContent': {
          templateUrl: "templates/volunteer.html"
        }
      }
    });

    var CCRiderObj = JSON.parse( localStorage.getItem('CCRider'));
    if(CCRiderObj) {
      //set default start to agenda
      $urlRouterProvider.otherwise('/app/livemap');
    } else {
      //set default start to settings
      $urlRouterProvider.otherwise('/app/settings');
    }
  }
);

Number.prototype.formatMoney = function(n,x){
  var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
      return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

setDataCache = function (name, data) {
  window.localStorage.setItem(name, JSON.stringify(data));
};

getDataCache = function (data) {
  return JSON.parse(window.localStorage.getItem(data));
};
