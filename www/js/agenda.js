angular.module('courage.controllers')
.controller('AgendaCtrl', function ($rootScope, $cordovaFile, $ionicPlatform, ParseService, ConnectivityMonitor) {
    var agenda = this;
    var sheetID = 2;

    $ionicPlatform.ready(function () {

      switch (ConnectivityMonitor.isOnline()){
        // ONLINE Branch
        case true:
          // Get live data for routes
          ParseService.getAgendas().then(function(data) {
            agenda.data = data;
            agenda.keys = Object.keys(data);
            agenda.selectDay(agenda.keys[0]);
          }).catch(function(error) {
            console.log(error);
          });
        break;

      // OFFLINE Branch
        case false:
          console.log('entered offline code');

          var cachedData = window.localStorage.getItem("agendas");
          agenda.data = cachedData;
          agenda.keys = Object.keys(cachedData);
          agenda.selectDay(agenda.keys[0]);
        break;
      }

    })

    agenda.selectDay = function (day) {
        this.selectedDay = day;
        agenda.currentEvents = agenda.data[day];
    }

    agenda.isActive = function (day) {
        return day === this.selectedDay;
    }

    agenda.toggleGroup = function(items) {
        if (agenda.isGroupShown(items)) {
            agenda.shownGroup = null;
        } else {
            agenda.shownGroup = items;
        }
    }

    agenda.isGroupShown = function(items) {
        return agenda.shownGroup === items;
    }
});
