angular.module('courage.controllers')
.controller('FAQCtrl', function ($scope, $rootScope, $cordovaFile, $ionicPlatform, ParseService, ConnectivityMonitor) {
    var faq = this;
    var sheetID = 7;
    $ionicPlatform.ready(function () {

      switch (ConnectivityMonitor.isOnline()){
        // ONLINE Branch
        case true:
          // Get live data for routes
          ParseService.getFAQs().then(function(data) {
            faq.data = data;
          }).catch(function(error) {
            console.log(error);
          });
        break;

      // OFFLINE Branch
        case false:
          console.log('entered offline code');

          var cachedData = window.localStorage.getItem("faq");
          faq.data = cachedData;
        break;
      }
    });

    faq.toggleGroup = function(items) {
        if (faq.isGroupShown(items)) {
            faq.shownGroup = null;
        } else {
            faq.shownGroup = items;
        }
    }

    faq.isGroupShown = function(items) {
        return faq.shownGroup === items;
    }
});
