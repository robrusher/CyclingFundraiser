angular.module('courage')

.factory('ParseService', function($q) {
  var factory = {};

  factory.getActivities = function() {
    var deferred = $q.defer();
    var ActivitiesTable = Parse.Object.extend("activity");
    var query = new Parse.Query(ActivitiesTable);
    query.ascending("day,order");
    query.find({
      success: function (parseResult) {
        setDataCache("activities",parseResult);
        var resultsString = JSON.stringify(parseResult);
        var results = getDataCache("activities");
        var groupedData = [];
        for(var d in results){
            if (typeof groupedData[results[d].day] == "undefined") {
                groupedData[results[d].day] = [];
            }
            groupedData[results[d].day].push(results[d]);
        };
        //console.log(groupedData);
        deferred.resolve(groupedData);
      },
      error: function (error) {
        deferred.reject(error);
      }
    });
    return deferred.promise;
  }

  factory.getAgendas = function() {
    var deferred = $q.defer();
    var AgendaTable = Parse.Object.extend("agenda");
    var query = new Parse.Query(AgendaTable);
    query.ascending("day,order");
    query.find({
      success: function (parseResult) {
        setDataCache("agendas",parseResult);
        var resultsString = JSON.stringify(parseResult);
        var results = getDataCache("agendas");
        var groupedData = [];
        for(var d in results){
            if (typeof groupedData[results[d].day] == "undefined") {
                groupedData[results[d].day] = [];
            }
            groupedData[results[d].day].push(results[d]);
        };
        //console.log(groupedData);
        deferred.resolve(groupedData);
      },
      error: function (error) {
        deferred.reject(error);
      }
    });
    return deferred.promise;
  }

  factory.getFAQs = function() {
    var deferred = $q.defer();
    var FAQTable = Parse.Object.extend("faq");
    var query = new Parse.Query(FAQTable);
    query.find({
      success: function (parseResult) {
        setDataCache("faqs",parseResult);
        var resultsString = JSON.stringify(parseResult);
        var results = getDataCache("faqs");
        var groupedData = [];
        for(var d in results){
            groupedData.push(results[d]);
        };
        //console.log(groupedData);
        deferred.resolve(groupedData);
      },
      error: function (error) {
        deferred.reject(error);
      }
    });
    return deferred.promise;
  }

  factory.getRestaurants = function() {
    var deferred = $q.defer();
    var DiningTable = Parse.Object.extend("dining");
    var query = new Parse.Query(DiningTable);
    query.ascending("day,activity");
    query.find({
      success: function (parseResult) {
        setDataCache("restaurants",parseResult);
        var resultsString = JSON.stringify(parseResult);
        var results = getDataCache("restaurants");
        var groupedData = [];
        for(var d in results){
            if (typeof groupedData[results[d].day] == "undefined") {
                groupedData[results[d].day] = [];
            }
            groupedData[results[d].day].push(results[d]);
        };
        //console.log(groupedData);
        deferred.resolve(groupedData);
      },
      error: function (error) {
        deferred.reject(error);
      }
    });
    return deferred.promise;
  }

  factory.getRoutes = function() {
    var deferred = $q.defer();
    var RoutesTable = Parse.Object.extend("route");
    var query = new Parse.Query(RoutesTable);
    query.ascending("day");
    query.ascending("markerset");
    query.find({
      success: function (parseResult) {
        setDataCache("routes",parseResult);
        var resultsString = JSON.stringify(parseResult);
        var results = getDataCache("routes");
        var groupedData = [];
        for(var d in results){
            if (typeof groupedData[results[d].day] == "undefined") {
                groupedData[results[d].day] = [];
            }
            groupedData[results[d].day].push(results[d]);
        };
        //console.log(groupedData);
        deferred.resolve(groupedData);
      },
      error: function (error) {
        deferred.reject(error);
      }
    });
    return deferred.promise;
  }

  factory.getStations = function() {
    var deferred = $q.defer();
    var StationTable = Parse.Object.extend("station");
    var query = new Parse.Query(StationTable);
    query.ascending("markerset");
    query.find({
      success: function (parseResult) {
        setDataCache("stations",parseResult);
        var resultsString = JSON.stringify(parseResult);
        var results = getDataCache("stations");
        var groupedData = [];
        for(var d in results){
            if (typeof groupedData[results[d].markerset] == "undefined") {
                groupedData[results[d].markerset] = [];
            }
            groupedData[results[d].markerset].push(results[d]);
        };
        //console.log(groupedData);
        deferred.resolve(groupedData);
      },
      error: function (error) {
        deferred.reject(error);
      }
    });
    return deferred.promise;
  }

  factory.getSponsors = function() {
    var deferred = $q.defer();
    var SponsorTable = Parse.Object.extend("sponsor");
    var query = new Parse.Query(SponsorTable);
    query.addAscending("order,name");
    query.find({
      success: function (parseResult) {
        setDataCache("sponsors",parseResult);
        var resultsString = JSON.stringify(parseResult);
        var results = getDataCache("sponsors");
        var groupedData = [];
        for(var d in results){
            if (typeof groupedData[results[d].level] == "undefined") {
                groupedData[results[d].level] = [];
            }
            groupedData[results[d].level].push(results[d]);
        };
        //console.log(groupedData);
        deferred.resolve(groupedData);
      },
      error: function (error) {
        deferred.reject(error);
      }
    });
    return deferred.promise;
  }

  factory.getVolunteer = function(consId) {
    var deferred = $q.defer();
    var VolunteerTable = Parse.Object.extend("volunteer");
    var query = new Parse.Query(VolunteerTable);
    query.equalTo("consId", consId);
    query.find({
      success: function (parseResult) {
        setDataCache("volunteer",parseResult);
        var resultsString = JSON.stringify(parseResult);
        var results = getDataCache("volunteer");
        var groupedData = [];
        for(var d in results){
            groupedData.push(results[d]);
        };
        //console.log('Volunteer: ' + groupedData);
        deferred.resolve(groupedData);
      },
      error: function (error) {
        deferred.reject(error);
      }
    });
    return deferred.promise;
  }

  return factory;
});
