angular.module('courage')
.factory('userService', function() {
    var user = {};
    return {
        get: function() {
            return user = JSON.parse(localStorage.getItem('CCRider'));
        },
        set: function(riderObj) {
            localStorage.setItem('CCRider', JSON.stringify(riderObj));
            localStorage.setItem('riderSelected','true');
        },
        clear: function() {
            localStorage.removeItem('CCRider');
            localStorage.setItem('riderSelected','false');
        }
    };
});
