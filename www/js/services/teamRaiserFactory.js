angular.module('courage')
.factory('teamRaiser', function($http){
    var promise;
    var trService = {
        async: function(rider){
            promise = $http({
                url: '<your teamRaiser server url>',
                method: 'GET',
                params: {'method': 'getParticipants',
                         'api_key': 'open',
                         'response_format': 'json',
                         'fr_id': 1450,
                         'first_name': rider.first,
                         'last_name': rider.last,
                         'v': '1.0',
                         'list_ascending': true,
                         'list_sort_column': 'first_name'
                     }
            }).then(function(response) {
              var riders = [];
              if(response.data.getParticipantsResponse.totalNumberResults > 1) {
                  riders = response.data.getParticipantsResponse.participant;
              }
              if(response.data.getParticipantsResponse.totalNumberResults == 1) {
                  riders.push(response.data.getParticipantsResponse.participant);
                  //console.log('only one');
              }
              // TearmRaiser getParticipants response
              return riders;
            },
            function(error) { // optional
                console.log('getParticipants service not found');
            });
          // Return the promise to the controller
          return promise;
        }
    };
    return trService;
})

.factory('riderLeaderBoard', function($http){
    var promise;
    var rlbService = {
        async: function(){
            promise = $http({
                url: '<your teamRaiser server url>',
                method: 'GET',
                params: {'method': 'getTopParticipantsData',
                         'api_key': 'open',
                         'response_format': 'json',
                         'fr_id': 1450,
                         'v': '1.0'
                        }
            })
            .then(function(response) {
                var rideLeaders = []; // top 10 riders: total, name, id
                    rideLeaders = response.data.getTopParticipantsDataResponse.teamraiserData;
                return rideLeaders;
            },
            function(error) { // optional
                console.log('getTopParticipantsData service not found');
            });
          // Return the promise to the controller
          return promise;
        }
    };
    return rlbService;
})

.factory('teamLeaderBoard', function($http){
    var promise;
    var tlbService = {
        async: function(){
            promise = $http({
                url: '<your teamRaiser server url>',
                method: 'GET',
                params: {'method': 'getTopTeamsData',
                         'api_key': 'open',
                         'response_format': 'json',
                         'fr_id': 1450,
                         'v': '1.0'
                        }
            })
            .then(function (response) {
                var teamLeaders = []; // top 10 teams: total, name, id
                    teamLeaders = response.data.getTopTeamsDataResponse.teamraiserData;
                return teamLeaders;
            },
            function (error) { // optional
                console.log('getTopTeamsData service not found');
            });
          // Return the promise to the controller
          return promise;
        }
    };
    return tlbService;
});
