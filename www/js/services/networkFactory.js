angular.module('courage')

.factory('ConnectivityMonitor', function($rootScope, $cordovaNetwork){

  return {
    isOnline: function(){
      // BUG caused switch to code below
      // if(ionic.Platform.isWebView()){
      //   return $cordovaNetwork.isOnline();
      // } else {
      //   return navigator.onLine;
      // }
      var haveInternet= true;
      if (window.cordova) {
          if (window.Connection) {
              if (navigator.connection.type == Connection.NONE) {
                  haveInternet= false;
              }
          }
      }
      return haveInternet;
    },
    isOffline: function(){
      if(ionic.Platform.isWebView()){
        return !$cordovaNetwork.isOnline();
      } else {
        return !navigator.onLine;
      }
    },
    startWatching: function(){
        if(ionic.Platform.isWebView()){

          $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            console.log("went online");
          });

          $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            console.log("went offline");
          });

        }
        else {

          window.addEventListener("online", function(e) {
            console.log("went online");
          }, false);

          window.addEventListener("offline", function(e) {
            console.log("went offline");
          }, false);
        }
    }
  }
});
