angular.module('courage.controllers')
.controller('ActivityCtrl', function ($rootScope, $cordovaFile, $ionicPlatform, ParseService, ConnectivityMonitor) {
    var activity = this;
    var sheetID = 3;

    $ionicPlatform.ready(function () {

      switch (ConnectivityMonitor.isOnline()){
        // ONLINE Branch
        case true:
          // Get live data for routes
          ParseService.getActivities().then(function(data) {
            activity.data = data;
            activity.keys = Object.keys(data);
            activity.selectDay(activity.keys[0]);
          }).catch(function(error) {
            console.log(error);
          });
        break;

      // OFFLINE Branch
        case false:
          console.log('entered offline code');

          var cachedData = window.localStorage.getItem("activities");
          activity.data = cachedData;
          activity.keys = Object.keys(cachedData);
          activity.selectDay(activity.keys[0]);
        break;
      }
    });

    activity.selectDay = function (day) {
        this.selectedDay = day;
        activity.currentEvents = activity.data[day];
    };

    activity.isActive = function (day) {
        return day === this.selectedDay;
    };

    activity.toggleGroup = function(items) {
        if (activity.isGroupShown(items)) {
            activity.shownGroup = null;
        } else {
            activity.shownGroup = items;
        }
    };

    activity.isGroupShown = function(items) {
        return activity.shownGroup === items;
    };
});
