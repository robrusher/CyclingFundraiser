angular.module('courage.controllers')
.controller('LivemapCtrl', function ($ionicPlatform, $scope, $http, leafletData, $ionicTabsDelegate, $rootScope, ParseService, ConnectivityMonitor) {
    var liveMap = this;
    var prevRoute;
    // create layer for markers
    var markers = new L.layerGroup();
    var rider = new L.layerGroup();

    $scope.$on('$ionicView.afterEnter', function() {
      ionic.trigger('resize');
    });

    $scope.defaults = {
      tileLayer: 'http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png',
      minZoom: 10,
      maxZoom: 17,
      zoomControl:false,
      tileLayerOptions: {
          detectRetina: true
      }
    };

    // define elevation control
    var el = L.control.elevation({
        position: "bottomright",
        theme: "steelblue-theme", //default: lime-theme
        width: window.innerWidth - 20,
        height: 95,
        margins: {
            top: 15,
            right: 20,
            bottom: 25,
            left: 57
        },
        useHeightIndicator: true, //if false a marker is drawn at map position
        interpolation: "linear", //see https://github.com/mbostock/d3/wiki/SVG-Shapes#wiki-area_interpolate
        hoverNumber: {
            decimalsX: 3, //decimals on distance (always in km)
            decimalsY: 0, //deciamls on height (always in m)
            formatter: undefined //custom formatter function may be injected
        },
        xTicks: undefined, //number of ticks in x axis, calculated by default according to width
        yTicks: undefined, //number of ticks on y axis, calculated by default according to height
        collapsed: false,    //collapsed mode, show chart on click or mouseover
        imperial: true
    });

    var base64shadow = 'img/markers/marker-shadow.png';

    // define icons
    var local_icons = {
        finish: {
          iconUrl: 'img/markers/flag.png',
          retinaUrl: 'img/markers/flag@2x.png',
          shadowUrl: base64shadow,
          iconSize: [28, 40],
          shadowSize: [40, 50],
          iconAnchor: [14, 40],
          shadowAnchor: [14, 50],
          popupAnchor: [0, -40]
        },
        lunch: {
          iconUrl: 'img/markers/life-saver.png',
          retinaUrl: 'img/markers/life-saver@2x.png',
          shadowUrl: base64shadow,
          iconSize: [28, 40],
          shadowSize: [40, 50],
          iconAnchor: [14, 40],
          shadowAnchor: [14, 50],
          popupAnchor: [0, -40]
        },
        aid: {
          iconUrl: 'img/markers/red-cross.png',
          retinaUrl: 'img/markers/red-cross@2x.png',
          shadowUrl: base64shadow,
          iconSize: [28, 40],
          shadowSize: [40, 50],
          iconAnchor: [14, 40],
          shadowAnchor: [14, 50],
          popupAnchor: [0, -40]
        },
        rider: {
          iconUrl: 'img/markers/blue.png',
          retinaUrl: 'img/markers/blue@2x.png',
          shadowUrl: base64shadow,
          iconSize: [28, 40],
          shadowSize: [40, 50],
          iconAnchor: [14, 40],
          shadowAnchor: [14, 50],
          popupAnchor: [0, -40]
        },
        relief: {
          iconUrl: 'img/markers/relief.png',
          retinaUrl: 'img/markers/relief@2x.png',
          shadowUrl: base64shadow,
          iconSize: [28, 40],
          shadowSize: [40, 50],
          iconAnchor: [14, 40],
          shadowAnchor: [14, 50],
          popupAnchor: [0, -40]
        },
        picture: {
          iconUrl: 'img/markers/camera.png',
          retinaUrl: 'img/markers/camera@2x.png',
          shadowUrl: base64shadow,
          iconSize: [28, 40],
          shadowSize: [40, 50],
          iconAnchor: [14, 40],
          shadowAnchor: [14, 50],
          popupAnchor: [0, -40]
        }
    };

    // liveMap.setDayData = function() {
    //   var today = new Date().toDateString();
    //   var day1 = new Date(2017,6,22).toDateString();
    //   var day2 = new Date(2017,6,23).toDateString();
    //
    //   switch(today){
    //     case day2:
    //       liveMap.selectDay(liveMap.keys[1]);
    //       liveMap.currentEvents = liveMap.data['SUN'];
    //       break;
    //     default:
    //       liveMap.selectDay(liveMap.keys[0]);
    //       liveMap.currentEvents = liveMap.data['SAT'];
    //   }
    // };

    // add elevation control to map
    leafletData.getMap().then(function(map) {
        el.addTo(map);
        map.addLayer(markers);
        map.addLayer(rider);
        map.attributionControl.setPrefix('');
    });

    liveMap.selectDay = function (day) {
        this.selectedDay = day;
        if( liveMap.data ){
            liveMap.currentEvents = liveMap.data[day];
            //liveMap.currentEventsTime = Object.keys(liveMap.currentEvents);
            setTimeout(function(){
                $ionicTabsDelegate.select(0);
                liveMap.loadGeoJSON(0);
            },100);
        }
    };

    liveMap.isActive = function (day) {
        return day === this.selectedDay;
    };

    liveMap.centerJSON = function() {
      leafletData.getMap().then(function(map) {
        var latlngs = [];
        for (var i in $scope.geojson.data.features[0].geometry.coordinates) {
          var coord = $scope.geojson.data.features[0].geometry.coordinates[i];
            //coord.pop();
            latlngs.push(L.GeoJSON.coordsToLatLng(coord));
        }
        map.fitBounds(latlngs,{padding:[20,20]});
      });
    };

    liveMap.getRiderLocation = function() {
        navigator.geolocation.getCurrentPosition(function(pos) {
            rider.clearLayers();
            var icon = L.icon(local_icons.rider);
            var you = new L.marker([pos.coords.latitude, pos.coords.longitude], {title: 'You are here.', icon: icon});
            rider.addLayer(you);
        }, function(error) {
            alert('Unable to get location: ' + error.message);
        });
    };

    // Add markers to map
    liveMap.addMarkers = function(set) {
            leafletData.getMap().then(function(map) {
                markers.clearLayers();
                var markerset = liveMap.sdata[set];
                for(var n in markerset) {
                    var icon = L.icon(local_icons[markerset[n].type]);
                    var marker = new L.marker([markerset[n].gpslocation.latitude,markerset[n].gpslocation.longitude], {icon: icon});
                    var popup = L.popup({maxWidth:300}).setContent(markerset[n].displaytext + '<br/>'+
                                     markerset[n].open + ' to ' +
                                     markerset[n].close);
                    marker.bindPopup(popup);
                    markers.addLayer(marker);
                }
            })
        liveMap.getRiderLocation();
    };

    // Get the geojson data from a JSON
    liveMap.loadGeoJSON = function(index) {
        var dayRoute = liveMap.currentEvents[index].file;
        var dayColor = liveMap.currentEvents[index].color;
        var fileName = "geojson/"+dayRoute;
        // clear elevation control if different route
        //if(dayRoute != prevRoute)
        el.clear();
        prevRoute = dayRoute;
        $http.get(fileName).success(function(data, status) {
          angular.extend($scope, {
            geojson: {
              data: data,
              style: {
                weight: 5,
                opacity: 1,
                color: dayColor
              },
              onEachFeature: el.addData.bind(el) // bind data to elevation control
            }
          });
        }).finally(function() {
          liveMap.addMarkers(liveMap.currentEvents[index].markerset);
          liveMap.centerJSON();
          setTimeout(function(){
              $ionicTabsDelegate.select(index);
          },100);
        });
    };

    $ionicPlatform.ready(function () {

      switch (ConnectivityMonitor.isOnline()){
        // ONLINE Branch
        case true:
          // Get live data for routes
          ParseService.getRoutes().then(function(data) {
            liveMap.data = data;
            liveMap.keys = Object.keys(data);
            liveMap.selectDay(liveMap.keys[0]);
          }).catch(function(error) {
            console.log(error);
          });
          // Get live data for stations
          ParseService.getStations().then(function(data) {
            liveMap.sdata = data;
            liveMap.skeys = Object.keys(data);
          }).catch(function(error) {
            console.log(error);
          });
        break;

      // OFFLINE Branch
        case false:
          console.log('entered offline code');

          var cachedData = window.localStorage.getItem("routes");
          liveMap.data = cachedData;
          liveMap.keys = Object.keys(cachedData);
          liveMap.selectDay(liveMap.keys[0]);

          var cachedSData = window.localStorage.getItem("stations");
          liveMap.sdata = cachedSData;
          liveMap.skeys = Object.keys(cachedSData);
        break;
      }

    });
});
