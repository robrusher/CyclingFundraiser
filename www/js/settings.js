angular.module('courage.controllers')
.controller('AppSettingsCtrl', function ($scope, $rootScope, $ionicPlatform, $ionicModal, teamRaiser, teamLeaderBoard, riderLeaderBoard, userService, ParseService, ConnectivityMonitor) {
    var appSettings = this;

    $ionicModal.fromTemplateUrl('templates/search.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });

    appSettings.updateRider = function(){
        teamRaiser.async(appSettings.riderObj.name).then(function(d){
            appSettings.selectRider(d[0]);
        }).catch(function(error) {
          console.log(error);
        });
    }

    appSettings.getParticipants = function(){
        teamRaiser.async(appSettings.rider).then(function(results){
            appSettings.riderList = results;
        }).catch(function(error) {
          console.log(error);
        });
        $scope.modal.show();
    };

    appSettings.getRiderLB = function(){
        riderLeaderBoard.async().then(function(d){
            appSettings.riderLB = d;
        });
    };

    appSettings.getTeamLB = function(){
        teamLeaderBoard.async().then(function(d){
            appSettings.teamLB = d;
        });
    };

    appSettings.selectRider = function (selectedRider) {
        // var user = $ionicUser;
        // user.details.name = selectedRider.name.first + ' ' + selectedRider.name.last;
        // user.set('first_name',selectedRider.name.first);
        // user.set('team',selectedRider.teamName);
        // user.set('captain',selectedRider.aTeamCaptain);
        // user.set('consId',Number(selectedRider.consId));
        // user.save();
        appSettings.riderList = null;
        selectedRider.goalp = selectedRider.goal;
        selectedRider.amountRaisedp = selectedRider.amountRaised;
        appSettings.congrats = selectedRider.amountRaised >= selectedRider.goal;
        selectedRider.goal = Number(selectedRider.goal/100).formatMoney(2,3);
        selectedRider.amountRaised = Number(selectedRider.amountRaised/100).formatMoney(2,3);

        userService.set(selectedRider);
        appSettings.riderObj = selectedRider;
        appSettings.riderSelected = true;
        //console.log(appSettings);
        $scope.modal.hide();
        ParseService.getVolunteer(Number(selectedRider.consId)).then(function(data) {
          $rootScope.volunteer = getDataCache("volunteer");
        }).catch(function(error) {
          console.log(error);
        });
    };

    appSettings.clearRider = function() {
      appSettings.rider = {};
      appSettings.riderObj = {};
      appSettings.riderSelected = false;
      $rootScope.volunteer = null;
      userService.clear();
      localStorage.removeItem("volunteer");
    };

    $ionicPlatform.ready(function () {
        appSettings.riderObj = userService.get();
        appSettings.riderSelected = localStorage.getItem('riderSelected');
        appSettings.isOnline = false;
        if(ConnectivityMonitor.isOnline()){
          appSettings.isOnline = true;
          appSettings.getTeamLB();
          appSettings.getRiderLB();
          if(userService.get()) {
            appSettings.updateRider();
          }
        };
    });

});
